# https://git-scm.com/docs/gitignore
# http://augustl.com/blog/2009/global_gitignores
# https://github.com/github/gitignore
# https://www.toptal.com/developers/gitignore

# Lines starting with `#` are comments.

# Ignore files called 'file.ext'
# file.ext

# Comments can't be on the same line as rules!
# The following line ignores files called 'file.ext # not a comment'
# file.ext # not a comment 

# Ignoring files with full path.
# This matches files in the root directory and subdirectories too.
# i.e. otherfile.ext will be ignored anywhere on the tree.
# dir/otherdir/file.ext
# otherfile.ext

# Ignoring directories
# Both the directory itself and its contents will be ignored.
# bin/
# gen/

# Glob pattern can also be used here to ignore paths with certain characters.
# For example, the below rule will match both build/ and Build/
# [bB]uild/

# Without the trailing slash, the rule will match a file and/or
# a directory, so the following would ignore both a file named `gen`
# and a directory named `gen`, as well as any contents of that directory
# bin
# gen

# Ignoring files by extension
# All files with these extensions will be ignored in
# this directory and all its sub-directories.
# *.apk
# *.class

# It's possible to combine both forms to ignore files with certain
# extensions in certain directories. The following rules would be
# redundant with generic rules defined above.
# java/*.apk
# gen/*.class

# To ignore files only at the top level directory, but not in its
# subdirectories, prefix the rule with a `/`
# /*.apk
# /*.class

# To ignore any directories named DirectoryA 
# in any depth use ** before DirectoryA
# Do not forget the last /, 
# Otherwise it will ignore all files named DirectoryA, rather than directories
# **/DirectoryA/

# This would ignore 
# DirectoryA/
# DirectoryB/DirectoryA/ 
# DirectoryC/DirectoryB/DirectoryA/
# It would not ignore a file named DirectoryA, at any level

# To ignore any directory named DirectoryB within a 
# directory named DirectoryA with any number of 
# directories in between, use ** between the directories
# DirectoryA/**/DirectoryB/

# This would ignore 
# DirectoryA/DirectoryB/ 
# DirectoryA/DirectoryQ/DirectoryB/ 
# DirectoryA/DirectoryQ/DirectoryW/DirectoryB/

# To ignore a set of files, wildcards can be used, as can be seen above.
# A sole '*' will ignore everything in your folder, including your .gitignore file.
# To exclude specific files when using wildcards, negate them.
# So they are excluded from the ignore list:
# !.gitignore 

# Use the backslash as escape character to ignore files with a hash (#)
# (supported since 1.6.2.1)
# \#*#

###################################
# Common
###################################

# ignore all logs *.log

# Packages #
############
# it's better to unpack these files and commit the raw source
# git has its own built in compression methods
#*.7z
*.dmg
#*.gz
*.iso
#*.jar
#*.rar
#*.tar
#*.zip

###################################
# GPG
###################################
# https://github.com/github/gitignore/blob/master/Global/GPG.gitignore

secring.*

###################################
# PuTTY
###################################
# https://github.com/github/gitignore/blob/master/Global/PuTTY.gitignore

# Private key
*.ppk

###################################
# Linux
###################################
# https://github.com/github/gitignore/blob/master/Global/Linux.gitignore

*~

# temporary files which can be created if a process still has a handle open of a deleted file
.fuse_hidden*

# KDE directory preferences
.directory

# Linux trash folder which might appear on any partition or disk
.Trash-*

# .nfs files are created when an open file is removed but is still being accessed
.nfs*

###################################
# OSX - macOS
###################################
# General
.DS_Store
.DS_Store?
.AppleDouble
.LSOverride

# Icon must end with two \r
Icon

# Thumbnails
._*
ehthumbs.db
Thumbs.db

# Files that might appear in the root of a volume
.DocumentRevisions-V100
.fseventsd
.Spotlight-V100
.TemporaryItems
.Trashes
.VolumeIcon.icns
.com.apple.timemachine.donotpresent

# Directories potentially created on remote AFP share
.AppleDB
.AppleDesktop
Network Trash Folder
Temporary Items
.apdisk

### macOS Patch ###
# iCloud generated files
*.icloud

###################################
# Windows
###################################
# https://github.com/github/gitignore/blob/master/Global/Windows.gitignore

# Windows thumbnail cache files
Thumbs.db
Thumbs.db:encryptable
ehthumbs.db
ehthumbs_vista.db

# Dump file
*.stackdump

# Folder config file
[Dd]esktop.ini

# Recycle Bin used on file shares
$RECYCLE.BIN/

# Windows Installer files
# *.cab
# *.msi
# *.msix
# *.msm
# *.msp

# Windows shortcuts
*.lnk


###################################
# Microsoft Office
###################################
# https://github.com/github/gitignore/blob/master/Global/MicrosoftOffice.gitignore

*.tmp

# Word temporary
~$*.doc*

# Word Auto Backup File
Backup of *.doc*

# Excel temporary
~$*.xls*

# Excel Backup File
*.xlk

# PowerPoint temporary
~$*.ppt*

# Visio autosave temporary files
*.~vsd*

# https://stackoverflow.com/questions/61811801/how-to-make-git-ignore-temporary-files-created-by-ms-office-starting-with
To ignore any file starting with ~$, use the pattern:
~$*

###################################
# LibreOffice
###################################
# https://github.com/github/gitignore/blob/master/Global/LibreOffice.gitignore

# LibreOffice locks
.~lock.*#

###################################
# Packer
###################################
# https://github.com/github/gitignore

# Cache objects
packer_cache/

# Crash log
crash.log

# https://www.packer.io/guides/hcl/variables
# Exclude all .pkrvars.hcl files, which are likely to contain sensitive data,
# such as password, private keys, and other secrets. These should not be part of
# version control as they are data points which are potentially sensitive and
# subject to change depending on the environment.
#
*.pkrvars.hcl

# For built boxes
*.box

### Packer Patch ###
# ignore temporary output files
output-*/

###################################
# Ansible
###################################
# https://github.com/ansible/ansible/blob/devel/.gitignore
# https://github.com/github/gitignore/blob/master/Global/Ansible.gitignore

*.retry

# manpage build stuff...
docs/man/man1/ansible*.1
docs/man/man1/ansible*.1.asciidoc.in

 Backwards compatibility with `stable-2.9` and earlier branches.
# Also used in the `devel` branch during early Ansible 2.10 development.
/lib/ansible.egg-info/
# First used in the `devel` branch during Ansible 2.10 development.
/lib/ansible_base.egg-info/
# First used in the `devel` branch during Ansible 2.11 development.
/lib/ansible_core.egg-info/
# vendored lib dir
lib/ansible/_vendor/*
!lib/ansible/_vendor/__init__.py

# test stuff
/test/integration/cloud-config-*.*
!/test/integration/cloud-config-*.*.template
.python-version
# Release directory
packaging/release/ansible_release
/.cache/
/test/results/
/test/integration/cloud-config-aws.yml
/test/integration/inventory.networking
/test/integration/inventory.winrm
/test/integration/cloud-config-aws.yml
/test/integration/cloud-config-cs.ini
# python 'rope' stuff
.ropeproject
# local 'ack' config files
.ackrc
# default 'coverage html' results
htmlcov/
# default 'coverage' tool data
.coverage
# ansible-test coverage results
test/units/.coverage.*
/test/integration/cloud-config-azure.yml
/SYMLINK_CACHE.json
changelogs/.plugin-cache.yaml
.ansible-test-timeout.json

###################################
# Pulumi
###################################
# Ignore temp build directory for Pulumi
# Info: https://www.pulumi.com/docs/

.pulumi/

###################################
# Terraform
###################################
# https://github.com/github/gitignore/blob/main/Terraform.gitignore

# Local .terraform directories
**/.terraform/*

# .tfstate files
*.tfstate
*.tfstate.*

# Crash log files
crash.log
crash.*.log

# Exclude all .tfvars files, which are likely to contain sensitive data, such as
# password, private keys, and other secrets. These should not be part of version 
# control as they are data points which are potentially sensitive and subject 
# to change depending on the environment.
*.tfvars
*.tfvars.json

# Ignore override files as they are usually used to override resources locally and so
# are not checked in
override.tf
override.tf.json
*_override.tf
*_override.tf.json

# Include override files you do wish to add to version control using negated pattern
# !example_override.tf

# Include tfplan files to ignore the plan output of command: terraform plan -out=tfplan
# example: *tfplan*

# Ignore CLI configuration files
.terraformrc
terraform.rc

###################################
# Dropbox
###################################
# https://github.com/github/gitignore/blob/master/Global/Dropbox.gitignore

# Dropbox settings and caches
.dropbox
.dropbox.attr
.dropbox.cache

###################################
# Emacs
###################################
# https://github.com/github/gitignore/blob/master/Global/Emacs.gitignore

# -*- mode: gitignore; -*-
*~
\#*\#
/.emacs.desktop
/.emacs.desktop.lock
*.elc
auto-save-list
tramp
.\#*

# Org-mode
.org-id-locations
*_archive

# flymake-mode
*_flymake.*

# eshell files
/eshell/history
/eshell/lastdir

# elpa packages
/elpa/

# reftex files
*.rel

# AUCTeX auto folder
/auto/

# cask packages
.cask/
dist/

# Flycheck
flycheck_*.el

# server auth directory
/server/

# projectiles files
.projectile

# directory configuration
.dir-locals.el

# network security
/network-security.data

###################################
# Vagrant
###################################
# https://github.com/github/gitignore/blob/master/Global/Vagrant.gitignore

# General
.vagrant/

# Log files (if you are creating logs in debug mode, uncomment this)
# *.log

###################################
# Vim
###################################
# https://github.com/github/gitignore/blob/master/Global/Vim.gitignore

# Swap
[._]*.s[a-v][a-z]
!*.svg  # comment out if you don't need vector files
[._]*.sw[a-p]
[._]s[a-rt-v][a-z]
[._]ss[a-gi-z]
[._]sw[a-p]

# Session
Session.vim
Sessionx.vim

# Temporary
.netrwhist
*~
# Auto-generated tag files
tags
# Persistent undo
[._]*.un~

###################################
# Xcode
###################################
# https://github.com/github/gitignore/blob/master/Global/Xcode.gitignore

# Xcode
#
# gitignore contributors: remember to update Global/Xcode.gitignore, Objective-C.gitignore & Swift.gitignore

## User settings
xcuserdata/

## compatibility with Xcode 8 and earlier (ignoring not required starting Xcode 9)
*.xcscmblueprint
*.xccheckout

## compatibility with Xcode 3 and earlier (ignoring not required starting Xcode 4)
build/
DerivedData/
*.moved-aside
*.pbxuser
!default.pbxuser
*.mode1v3
!default.mode1v3
*.mode2v3
!default.mode2v3
*.perspectivev3
!default.perspectivev3

## Gcc Patch
/*.gcno

###################################
# Visual Studio Code
###################################
# https://github.com/github/gitignore/blob/master/Global/VisualStudioCode.gitignore

.vscode/*
!.vscode/settings.json
!.vscode/tasks.json
!.vscode/launch.json
!.vscode/extensions.json
!.vscode/*.code-snippets

# Local History for Visual Studio Code
.history/

# Built Visual Studio Code Extensions
*.vsix

###################################
# IntelliJ IDEA stuff..
###################################

# Covers JetBrains IDEs: IntelliJ, RubyMine, PhpStorm, AppCode, PyCharm, CLion, Android Studio, WebStorm and Rider
# Reference: https://intellij-support.jetbrains.com/hc/en-us/articles/206544839

# User-specific stuff
.idea/**/workspace.xml
.idea/**/tasks.xml
.idea/**/usage.statistics.xml
.idea/**/dictionaries
.idea/**/shelf

# AWS User-specific
.idea/**/aws.xml

# Generated files
.idea/**/contentModel.xml

# Sensitive or high-churn files
.idea/**/dataSources/
.idea/**/dataSources.ids
.idea/**/dataSources.local.xml
.idea/**/sqlDataSources.xml
.idea/**/dynamic.xml
.idea/**/uiDesigner.xml
.idea/**/dbnavigator.xml

# Gradle
.idea/**/gradle.xml
.idea/**/libraries

# Gradle and Maven with auto-import
# When using Gradle or Maven with auto-import, you should exclude module files,
# since they will be recreated, and may cause churn.  Uncomment if using
# auto-import.
# .idea/artifacts
# .idea/compiler.xml
# .idea/jarRepositories.xml
# .idea/modules.xml
# .idea/*.iml
# .idea/modules
# *.iml
# *.ipr

# CMake
cmake-build-*/

# Mongo Explorer plugin
.idea/**/mongoSettings.xml

# File-based project format
*.iws

# IntelliJ
out/

# mpeltonen/sbt-idea plugin
.idea_modules/

# JIRA plugin
atlassian-ide-plugin.xml

# Cursive Clojure plugin
.idea/replstate.xml

# SonarLint plugin
.idea/sonarlint/

# Crashlytics plugin (for Android Studio and IntelliJ)
com_crashlytics_export_strings.xml
crashlytics.properties
crashlytics-build.properties
fabric.properties

# Editor-based Rest Client
.idea/httpRequests

# Android studio 3.1+ serialized cache file
.idea/caches/build_file_checksums.ser

### Intellij Patch ###
# Comment Reason: https://github.com/joeblau/gitignore.io/issues/186#issuecomment-215987721

# *.iml
# modules.xml
# .idea/misc.xml
# *.ipr

# Sonarlint plugin
# https://plugins.jetbrains.com/plugin/7973-sonarlint
.idea/**/sonarlint/

# SonarQube Plugin
# https://plugins.jetbrains.com/plugin/7238-sonarqube-community-plugin
.idea/**/sonarIssues.xml

# Markdown Navigator plugin
# https://plugins.jetbrains.com/plugin/7896-markdown-navigator-enhanced
.idea/**/markdown-navigator.xml
.idea/**/markdown-navigator-enh.xml
.idea/**/markdown-navigator/

# Cache file creation bug
# See https://youtrack.jetbrains.com/issue/JBR-2257
.idea/$CACHE_FILE$

# CodeStream plugin
# https://plugins.jetbrains.com/plugin/12206-codestream
.idea/codestream.xml

# Azure Toolkit for IntelliJ plugin
# https://plugins.jetbrains.com/plugin/8053-azure-toolkit-for-intellij
.idea/**/azureSettings.xml

###################################
# Java
###################################
# https://github.com/dclong/xinstall/blob/dev/xinstall/data/git/gitignore_java

# Java 
*.class

## BlueJ files
*.ctxt

## Mobile Tools for Java (J2ME)
.mtj.tmp/

## Package Files
*.jar
*.war
*.ear

# Gradle 
.gradle
/build/
/out/

## Ignore Gradle GUI config
gradle-app.setting

## Avoid ignoring Gradle wrapper jar file (.jar files are usually ignored)
!gradle-wrapper.jar

## Cache of project
.gradletasknamecache

# virtual machine crash logs, see http://www.java.com/en/download/help/error_hotspot.xml
hs_err_pid*

# IDE
.theia/
.idea/
.vscode/
*.ipr
*.iws

# Misc
core
*.log
deprecated

###################################
# Python
###################################
# https://github.com/dclong/xinstall/blob/dev/xinstall/data/git/gitignore_python
.idea/
.theia/
.vscode/
*.ipr
*.iws
.coverage
.mypy/
.mypy_cache/
.pytype/
*.crc
__pycache__/
venv/
.venv/
target/
dist/
*.egg-info/
doc*/_build/
*.prof
core

# gitginore template for creating Snap packages
# website: https://snapcraft.io/

parts/
prime/
stage/
*.snap

# Snapcraft global state tracking data(automatically generated)
# https://forum.snapcraft.io/t/location-to-save-global-state/768
/snap/.snapcraft/

# Source archive packed by `snapcraft cleanbuild` before pushing to the LXD container
/*_source.tar.bz2

# gitignore template for Jupyter Notebooks
# website: http://jupyter.org/

# Remove previous ipynb_checkpoints
#   git rm -r .ipynb_checkpoints/
.ipynb_checkpoints
*/.ipynb_checkpoints/*

# IPython
profile_default/
ipython_config.py

###################################
# Rust
###################################
# https://github.com/github/gitignore/blob/main/Rust.gitignore

# Generated by Cargo
# will have compiled files and executables
debug/
target/

# Remove Cargo.lock from gitignore if creating an executable, leave it for libraries
# More information here https://doc.rust-lang.org/cargo/guide/cargo-toml-vs-cargo-lock.html
Cargo.lock

# These are backup files generated by rustfmt
**/*.rs.bk

# MSVC Windows builds of rustc generate these, which store debugging information
*.pdb

###################################
# Chef Cookbook
###################################
# https://github.com/github/gitignore/blob/main/ChefCookbook.gitignore

.vagrant
/cookbooks

# Bundler
bin/*
.bundle/*

.kitchen/
.kitchen.local.yml

###################################
# Chef Cookbook
###################################
# https://github.com/github/gitignore/blob/main/WordPress.gitignore

# Wordpress - ignore core, configuration, examples, uploads and logs.
# https://github.com/github/gitignore/blob/main/WordPress.gitignore

# Core
#
# Note: if you want to stage/commit WP core files
# you can delete this whole section/until Configuration.
/wp-admin/
/wp-content/index.php
/wp-content/languages
/wp-content/plugins/index.php
/wp-content/themes/index.php
/wp-includes/
/index.php
/license.txt
/readme.html
/wp-*.php
/xmlrpc.php

# Configuration
wp-config.php

# Example themes
/wp-content/themes/twenty*/

# Example plugin
/wp-content/plugins/hello.php

# Uploads
/wp-content/uploads/

# Log files
*.log

# htaccess
/.htaccess

# All plugins
#
# Note: If you wish to whitelist plugins,
# uncomment the next line
#/wp-content/plugins

# All themes
#
# Note: If you wish to whitelist themes,
# uncomment the next line
#/wp-content/themes

###################################
# Podman
###################################
# https://github.com/containers/podman/blob/main/.gitignore

/.artifacts/
/bin/
/brew
/build/
/conmon/
contrib/spec/podman.spec
contrib/systemd/*/*.service
*.coverprofile
coverprofile
/.coverage
/docs/*.[158]
/docs/*.[158].gz
/docs/build/
/docs/remote
**.DS_Store
.gopathok
.idea*
.nfs*
*.o
*.orig
/_output/
/podman_tmp_*
/pause/pause.o
pkg/api/swagger.yaml
/podman-remote*.zip
/podman*.tar.gz
/podman-*.msi
__pycache__
release.txt
.ropeproject
*.rpm
/test/bin2img/bin2img
/test/checkseccomp/checkseccomp
/test/copyimg/copyimg
/test/goecho/goecho
/test/version/version
/test/testvol/testvol
/test/tools/build
/test/e2e/ginkgo-node-*
.vscode*
tags
result
# Necessary to prevent hack/tree-status.sh false-positive
/*runner_stats.log
.generate-bindings
